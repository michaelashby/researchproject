@extends('layouts.master')
@section('title', 'Edge Hill University')
@section('content')
<html>
  <head>
    <center><div class="box black"><h1>Interactive Slider<h1></div></center>
      <h4>Users would like an interactive slider placed here. This would well benefit intial
        user interaction on the website. Users would be able to slide both left and right brand
        click their desired location for the site. The images should represent the main pages on the
        site. These would lead directly to the Courses, Staff and Contact page. This will also
        be the first set of quick links on the website. An example of what this could look like
        can be seen if the chosen high-fidelity design  is referred to in the project report.
      </h4>
    </head>
    <body>
        <h1>Welcome to Edge Hill University</h1></br>
          <h4>Welcome to the Edge Hill University computing department website. On here you will find all
          the information you need. Find out all the latest information on courses, modules and all
          entry requirements. We offer a wide range of computer based courses that will help towards
          securing that career of you choice. We hope that we can help you acheive that goal.
          For more information call us on 01695 575171. </h4>

        <div>
          <h2 class="pull-left">Course Search</h2>
          <h2 class="pull-right">Computing News</h2>
        </div></br></br></br></br>
          <center><p>Above are the two interactive features of the home page. During the research process
          the users confirmed that they liked the search bar option to find their and would like to keep it.
          However, it was suggested that when they are typing the name of the course a suggestion in a list
          displays. This will enable them to gain access to the page quicker and removes a click process. The
          second interactive option will be a regular updated news feed. Users will be able click and expand
          on a news story and it will take them directly to that page. Users would like regualr news updates on
          the page so it makes it interesing to read.</p></center>
  </body>
