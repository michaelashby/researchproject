<nav class="navbar navbar-inverse navbar-fixed-top">
     <div class="container-fluid">
          <ul class="nav navbar-nav">
             <a class="navbar-brand" href="/">Edge Hill University</a>
             <li class="active"><a href="/courses">Courses</a></li>
             <li><a href="/staff">Staff</a></li>
             <li><a href="/contact">Contact Us</a></li>
         </ul>
     </div>
 </nav>
